import './../sass/styles.scss'; 
var desperdicio = {
    segundos: 4,
    minutos: 242,
    horas: 14498,
    dias: 347945
}
var currentKG = 0;
var initDate = new Date();
var isMobile;
var scrollCtrl = (() => {
    var currsetSection = 'cover';
    var firstTouch, lastDelta;
    function touchStart(e) {
        scrollCtrl.firstTouch = e.touches[0].pageY
    }
    function touchMove (e) {
        scrollCtrl.lastDelta = e.touches[0].pageY
        
    }
    function touchEnd(e){
      var scrollTop = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0)      
      var delta = scrollCtrl.firstTouch - scrollCtrl.lastDelta;
      delta = scrollCtrl.firstTouch > scrollCtrl.lastDelta ? 20 : -20
      paralax(scrollTop, scrollTop, delta);
    }
    function preventDefault(e) {
        e.preventDefault()
  
        var delta = e.wheelDelta || -e.deltaY;
        if (testBrowser('firefox') && e.deltaMode === 1) {
            if (delta < 0) {
                if (delta > -180) delta = -180
            } else {
                if (delta < 180) delta = 180
            }
        } else if (testBrowser('firefox')) {
            delta -= delta * 0.80
        }
        delta = e.wheelDelta ? delta / 120 : delta / 3;
        scrollCtrl.scroll(delta)
    }
  
    function scroll(delta) {
        var limit = window.innerHeight / 5
        /*if(typeof InstallTrigger !== 'undefined') // is firefox
          delta += delta * 5.5;*/
        this.lastDelta = delta
    
        // if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 && e.deltaMode==1){
        if (delta < 0) {
          if (delta < -limit) delta = -limit
        } else {
          if (delta > limit) delta = limit
        }
        // }
    
        var scrollTop = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0)
        var finalScroll = scrollTop - parseInt(delta*170);
        paralax(scrollTop, finalScroll, delta);
        TweenMax.to(window, 0.8, {
          scrollTo: { y: finalScroll, autoKill: true },
          ease: Power4.easeOut,
          overwrite: 5
        })
    }
    function paralax(scrollTop, finalScroll, delta){
        var offs = scrollCtrl.currsetSection == 'cover' ? 800 : 300;   
        if(isMobile)    
            offs = scrollCtrl.currsetSection == 'cover' ? 180 : 120;               
             
        var section = Array.from($('.section-with-bg')).find( (s) => { return finalScroll >= s.offsetTop - offs && finalScroll <= s.offsetTop + s.offsetHeight - offs; });
        if(section)
            section = $(section).data('bg');
        if(section && scrollCtrl.currsetSection != section){
            $('.bg-section[data-name="' + scrollCtrl.currsetSection + '"] *:not(g)').each((i, o)=>{
                TweenMax.to(o, Math.random() * (6 - 1) + 1, {
                    y: -2 * (i > 2 ? 2 : i),
                    opacity: 0,
                    ease: Power4.easeOut,
                });
            });
            $('.bg-section[data-name="' + section + '"]').addClass('active').siblings().removeClass('active');
            $('.bg-section[data-name="' + section + '"] *:not(g)').each((i, o)=>{
                TweenMax.to(o, Math.random() * (6 - 1) + 1, {
                    opacity: 1,
                    y: scrollTop <= 0 ? 0 : (delta * 0.2 + 70 * (i > 5 ? 5 : i)),
                    ease: Power4.easeOut,
                });
            });
            if(section == 'photo1')
                TweenMax.to($('#gradient2')[0], 0.3, {attr:{x2: 984}, delay: 0.5, ease: Power4.easeOut});
            else
                TweenMax.to($('#gradient2')[0], 0.3, {attr:{x2: 1608}, delay: 0.5, ease: Power4.easeOut});                
            scrollCtrl.currsetSection = section;
        }else{
            $('.bg-section[data-name="' + scrollCtrl.currsetSection + '"] *:not(g)').each((i, o)=>{
                var y = scrollTop <= 0 ? 0 : (delta * 100 + 70 * (i > 10 ? 10 : i));
                if((scrollCtrl.currsetSection == 'cover' && scrollTop > 0))
                    y = (delta * 0.1 + 20 * (i > 5 ? 5 : i));
                if(isMobile)
                    y = (delta * 0.8 + 70 * (i > 5 ? 5 : i));
                TweenMax.to(o, Math.random() * (6 - 1) + 1, {
                    y: y,
                    ease: Power4.easeOut,
                });
            });
        }
    }
    return {
        preventDefault: preventDefault,
        touchStart: touchStart,
        touchEnd: touchEnd,
        touchMove: touchMove,
        currsetSection: currsetSection,
        firstTouch: firstTouch, 
        lastDelta: lastDelta,
        scroll: scroll
    }
})();
$(document).ready(()=> {
    init();
    $('.bg-section:first *:not(g)').each((i, o)=>{
        TweenMax.fromTo(o, Math.random() * (6 - 1) + 1, {y: - 70 * (i > 10 ? 5 : i)}, {
            y: '0',
            opacity: 1,
            ease: Power4.easeOut,
            delay: 0.2
        });
    });
});
var loading = true;
function init(){
    isMobile = $(window).width() <= 430;
    if (window.addEventListener) window.addEventListener('DOMMouseScroll', scrollCtrl.preventDefault, {passive: true})
    window.onwheel = scrollCtrl.preventDefault // modern standard
    window.onmousewheel = document.onmousewheel = scrollCtrl.preventDefault // older browsers, IE
    
    document.onkeydown = scrollCtrl.preventDefaultForScrollKeys
    
    window.addEventListener('touchmove', scrollCtrl.touchMove, {passive: true})
    window.addEventListener('touchstart', scrollCtrl.touchStart, {passive: true})
    window.addEventListener('touchend', scrollCtrl.touchEnd, {passive: true})
    /*var tween = new TimelineMax().add(
        TweenMax.to($('#loading svg .border')[0], 2, {strokeDasharray: $('#loading svg .border')[0].getTotalLength(), strokeDashoffset: $('#loading svg .border')[0].getTotalLength(), ease: Power2.easeInOut}),
        TweenMax.to($('#loading svg .border')[0], 2, {strokeDashoffset: $('#loading svg .border')[0].getTotalLength() * 2, delay: 2.3, ease: Power2.easeInOut}),
        TweenMax.set($('#loading svg .border')[0], {strokeDasharray: 'none',strokeDashoffset: 0, delay: 4.3, ease: Power2.easeInOut})       
    );  
    tween.repeat(-1).repeatDelay(4.5).play();*/
    function loadingAnimation(){
        TweenMax.to($('#loading svg .border')[0], 2, {opacity: 0, strokeDasharray: $('#loading svg .border')[0].getTotalLength(), strokeDashoffset: $('#loading svg .border')[0].getTotalLength(), ease: Power2.easeInOut});
        TweenMax.to($('#loading svg .border')[0], 2, {strokeDashoffset: $('#loading svg .border')[0].getTotalLength() * 2, opacity: 1, delay: 2.3, ease: Power2.easeInOut});
        TweenMax.set($('#loading svg .border')[0], {strokeDasharray: 'none',strokeDashoffset: 0, delay: 4.3, ease: Power2.easeInOut});
        if(loading)
            setTimeout(() => {requestAnimationFrame(loadingAnimation)}, 4400);
        else
            cancelAnimationFrame(loadingID);
    }
    var loadingID = requestAnimationFrame(loadingAnimation);
    setTimeout(() => { loading = false; $('#loading').addClass('hide') }, 6000);
    
    currentKG = initDate.getHours() * desperdicio.horas; 
    setInterval(updateCounter, 1000 );
    $('#bgGradient').height($('body').height())
    $('.section-with-bg').each((i, o) => {
        var bg = $('#bgContainer svg[data-name="' + $(o).data('bg') + '"]');
        var top = $(o).offset().top// + Number(window.getComputedStyle(o).marginTop.replace(/px/, ''));
        bg.addClass('bg-section').css('top', top);
        //$(o).append(bg);
    });
    $('body').on('mousemove', (e) => {
        var x = 0.004 * (100 * (e.clientX) / 134 - 50)
        var y = 0.004 * (100 * (e.clientY) / 134 - 50)
        $('.bg-section[data-name="' + scrollCtrl.currsetSection + '"] *:not(g)').each((i, o)=>{
            TweenMax.to(o, Math.random() * (6 - 1) + 1, {
                x: x * (i > 10 ? 7 : i),
                ease: Power4.easeOut,
            });
        });
        TweenMax.to($('#cover figure')[0], 1.8, {
            x: x * -3,
            y: y,            
            ease: Power4.easeOut,
        });
    })
    $('header a').on('click', function(e) {
        e.preventDefault();
        $('#toggle').trigger('click');
        TweenMax.to(window, 0.8, {
            scrollTo: { y: $($(this).attr('href')).offset().top, autoKill: true },
            ease: Power4.easeOut,
            overwrite: 5
        })
    });
    $('#toggle').on('click', function(e) {
        $('header nav').toggleClass('openned');
        $(this).toggleClass('close');
        
        
        if(!$('header nav').hasClass('openned')){
            TweenMax.to($('header nav')[0], 0.5, {opacity: 0, ease: Power4.easeOut});
            $('#cover, header .logo, header .lang, .video').each((i, o)=>{TweenMax.to(o, 0.5, {opacity: 1, ease: Power4.easeOut})});            
        }else{
            $('#cover, header .logo, header .lang, .video').each((i, o)=>{TweenMax.to(o, 0.5, {opacity: 0, ease: Power4.easeOut})});
            TweenMax.fromTo($('header nav')[0], 0.5, {opacity: 0},{
                opacity: 1,
                ease: Power4.easeOut,
            });
            var delay = 0.5;
            $('header nav a').each((i, o)=>{
                TweenMax.fromTo(o, 0.3, {y: -10, opacity: 0},{
                    opacity: 1,
                    y: 0,
                    delay: delay,
                    ease: Power4.easeOut,
                });
                delay += 0.2;
            });
        }
    });
    $('header .lang').on('mouseenter', function(e) {
        if(!isMobile)
            $('header .lang').addClass('openned');        
    });
    $('header .lang').on('mouseleave', function(e) {
        $('header .lang').removeClass('openned');        
    });
    $('header .lang').on('click', function(e) {
        if(isMobile)
            $('header .lang').toggleClass('openned');
    });
    $('header .lang .option').on('click', function(e) {
        e.stopPropagation();
        var o = $(this).text();
        $(this).text($('header .lang .active').text());
        $('header .lang .active span').text(o);
        $('header .lang').removeClass('openned').trigger('mouseleave');
    });
    $('#wwd li').on('click', function(){
        var $active = $(this);
        var h = $active[0].scrollHeight;
        $active.height(h);
        setTimeout(() => {
            $active.addClass('active');
            $active.siblings().removeClass('active').height(62);
            $active.closest('.flex').find('figure[data-name="' + $active.data('name') + '"]').addClass('active').siblings().removeClass('active');
        }, 100);
    })
    
    setTimeout(() => {
        $('#wwd li h3').each((i, o) => {
            var w = $(o)[0].clientWidth;
            $(o).css('background-position-x', -w);
            $(o).css('background-size', w + 'px ' + '100%');
        });
        
    }, 500);
    //$('#wwd li.active').trigger('mouseenter');
} 
var isInit = true;
function updateCounter(){
    var incremento = desperdicio.segundos
    var num = currentKG + incremento;
    var formated = format(num);
    var html = '';
    var currentStr = format(currentKG);
	var indices = [currentStr.length-1];
	if(currentStr.length > 1)
    for (let i = currentStr.length-1; i >= 0 ; i--) {
		if(currentStr[i] == ',') continue;
        if(i == currentStr.length-1){
            if((Number(currentStr[i]) + incremento)>= 10)
                indices.push(i-1)
			else break;
        }else if(i != currentStr.length-2){
            if(currentStr[i+1] >= 9 || (currentStr[i+1] == ',' && currentStr[i+2] >= 9))
                indices.push(i)   
			else break;             
        }
    }
    //console.log(indices, num)
    var changeAll = isInit || currentStr.length < formated.length;
    var array = Array.from(formated);
    if(changeAll){
        $('#cover .counter .number span').css('opacity', 0);
        html = array.map((c) => {
            return '<span style="opacity: 0">' + c + '</span>';
        }).join('');
        
        $('#cover .counter .number').html(html);
        $('#cover .counter .units').css('transform', 'translateX(calc(' + ($('#cover .counter .number').width() - (isMobile ? 0 : 30)) + 'px - 50%))');        
        $('#cover .counter .number span').each(function(i, o){
            TweenMax.fromTo(o, 0.5, {y: 30, opacity: 0}, {y: 0, opacity: 1, delay: 0.2, ease: Power4.easeOut});
        });
    }else{
        var w = $('#cover .counter .number').width()
        $('#cover .counter .number span').each(function(i, o){
            if(indices.indexOf(i) != -1) 
                TweenMax.to(o, 0.3, {y: -20, opacity: 0, ease: Power4.easeOut, onComplete: function(){ 
                    $(o).text(formated[i]);
                    TweenMax.fromTo(o, 0.5, {y: 30, opacity: 0}, {y: 0, opacity: 1, delay: 0.2, ease: Power4.easeOut});                    
                }});
        });
        setTimeout(()=>{
            if(Math.abs($('#cover .counter .number').width() - w) > 10)
                $('#cover .counter .units').css('transform', 'translateX(calc(' + ($('#cover .counter .number').width() - (isMobile ? 0 : 30)) + 'px - 50%))');
        }, 1000);
        
        
        
        
    }
    currentKG = num;
    isInit = false;
}


function format(n) {
    var parts = n.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
}

function testBrowser (browser) {
    var result

    switch (browser) {
      case 'safari':
        result = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === '[object SafariRemoteNotification]' })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification))
        break
      case 'safari mobile':
        result = /iPhone/i.test(navigator.userAgent) && /Safari/i.test(navigator.userAgent)
        break
      case 'samsung':
        result = /SamsungBrowser/.test(navigator.userAgent)
        break
      case 'chrome':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent)
        break
      case 'chrome mobile':
        result = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !/SamsungBrowser/.test(navigator.userAgent) && !window.chrome.webstore
        break
      case 'firefox mobile':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent) && /Mobile/.test(navigator.userAgent)
        break
      case 'firefox':
        result = !/Chrome/.test(navigator.userAgent) && /Mozilla/.test(navigator.userAgent) && /Firefox/.test(navigator.userAgent)
        break
      case 'ie':
        result = /MSIE/.test(window.navigator.userAgent) || /NET/.test(window.navigator.userAgent);
        break;
      case 'edge':
        result = /Edge/.test(window.navigator.userAgent);
      default:
        result = false
        break
    }
    return result
}